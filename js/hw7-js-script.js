const usersList1 = [{
	name: "George",
	surname: "Clooney",
	gender: "male",
   age: 55
},
{
   name: "George",
	surname: "Michael",
	gender: "male",
	age: 55
},
{
   name: "Ashley",
   surname: "Gracham",
   gender: "female",
   age: 31
}];

const usersList2 = [{
	name: "George",
	surname: "Harrison",
	gender: "male",
	age: 64
},
{
	name: "George",
	surname: "Ezra",
	gender: "male",
	age: 25
},
{
   name: "Amal",
   surname: "Clooney",
   gender: "female",
   age: 41
}]

// перший варіант рішення з використанням тільки forEach та умовного оператора if

// const excludedUsers = []; // створили масив3 куда будуть передаватись об'єкти, що задовільняють умови

// usersList1.forEach( function(item1){       // нехай для кожного об'єкту масиву1
//    let flag = true;                        // ставимо флаг (булеве значення, яке присвоюємо перемінній)
//    usersList2.forEach( function(item2){    // poзглядати кожен об'єкт масиву2
//       if (item2.name == item1.name){       // якщо ім'я з масиву2 співпадає з ім'ям масиву1
//          flag = false;                     // тоді це неправдива умова
//       }
//    });
//       if (flag){                          // якщо ім'я з масиву1 не співпало з іменами в об'єктах масиву2
//          excludedUsers.push(item1)        // додати цей об'єкт з масиву1 в масив3
//       }
// });
// console.log( excludedUsers );              // вивести в консолі масив3






// другий варіант рішення з використанням arr.filter, forEach та умовного оператора if

// const excludedUsers = usersList1.filter( item => { 
//    let flag = true;
//    usersList2.forEach( item2 => {
//       if(item["name"] == item2["name"]){
//          flag = false
//       }
//    })
//    return flag
//    } );

// console.log( excludedUsers );






 // третій варіант рішення з використанням arr.filter та arr.every

const excludedUsers = usersList1.filter( item1 => { // присвоїти масиву3 об'єкт з масиву 1, який пройшов фільтр
   return usersList2.every( item2 => {              // для кожного об'єкту масиву2
      return item1.name != item2.name              // повернути об'єкт масиву 1 якщо його ім'я не співпадає з іменем масиву2
   });
});
console.log( excludedUsers );                       // вивести в консолі об'єкт/об'єтки масиву3